from flask import Flask, request
from flask_cors import CORS
from src.api import apiv1

app = Flask(__name__)
CORS(app, max_age=1728000)

app.register_blueprint(apiv1, url_prefix='/v1')