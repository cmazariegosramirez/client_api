from ..mongo_cnx import CollData, my_schema
import flask


def insert_data():
    js = flask.request.get_json(force=True, silent=True)
    data = CollData.from_json(js)

    my_schema.coll_data.insert_one(data.to_json())

    return ['',201]