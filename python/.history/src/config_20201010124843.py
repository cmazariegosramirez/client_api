import json

def resp(js, status):
    # return json.dumps(js) if status != 204 else "", status, {"Content-Type": "application/json","cache-control": "no-cache, no-store"}
    return json.dumps(js), status, {"Content-Type": "application/json", "Expires": 0}