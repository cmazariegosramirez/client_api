from ..mongo_cnx import CollPosts, my_schema
from .. import config
import flask
import requests


def get_posts():
    posts = list(my_schema.coll_posts.find({}).limit(10000) or [])

    return config.resp(posts,200)

def insert_posts():
    js = flask.request.get_json(force=True, silent=True)
    data = CollPosts.from_json(js)

    # To-do a buscar al otro servidor
    

    my_schema.coll_posts.insert_one(data.to_json())

    return config.resp({},201)