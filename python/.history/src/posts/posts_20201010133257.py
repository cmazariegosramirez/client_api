from ..mongo_cnx import CollPosts, my_schema
from .. import config
import flask
import requests


def get_posts():
    posts = list(my_schema.coll_posts.find({}).limit(10000) or [])

    return config.resp(posts,200)


def get_count_posts():
    count = my_schema.coll_posts.count_documents({})
    return config.resp({"count": count},200)

def insert_posts():
    js = flask.request.get_json(force=True, silent=True)
    data = CollPosts.from_json(js)

    # To-do a buscar al otro servidor
    for ip in config.servers:
        r = requests.get('http://' + ip + '/v1/data', auth=('user', 'pass'))
        rsp: dict = r.json()
        count = rsp.get('count',0)
        

    my_schema.coll_posts.insert_one(data.to_json())

    return config.resp({},201)