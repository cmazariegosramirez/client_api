from ..mongo_cnx import CollPosts, my_schema
from .. import config
import flask


def get_posts():
    js = flask.request.get_json(force=True, silent=True)
    data = CollPosts.from_json(js)

    my_schema.coll_posts.find_one(data.to_json())

    return config.resp({},201)
def insert_posts():
    js = flask.request.get_json(force=True, silent=True)
    data = CollPosts.from_json(js)

    my_schema.coll_posts.insert_one(data.to_json())

    return config.resp({},201)