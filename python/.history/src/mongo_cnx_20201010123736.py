import pymongo

mongocnx = pymongo.MongoClient("mongodb://localhost:27017/")


class CollData:
    def __init__(self, autor: str, nota: str ) -> None:
        self.autor = ''
        self.nota = ''
    
    def getJSON(self):
        return { "autor": self.autor, "nota": self.nota }


class MySchema:
    def __init__(self) -> None:
        global mongocnx
        self.cnx = mongocnx
        self.db = mongocnx['sopes1']
        self.coll_data = self.db['data']

my_schema = MySchema()