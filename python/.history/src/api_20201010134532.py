import os, flask
from src.objects.C2Blueprint import C2Blueprint
from .posts import posts

apiv1 = C2Blueprint("private_v1", __name__)


apiv1.add_url_rule('/posts', posts.get_posts, methods=['GET'])
apiv1.add_url_rule('/posts/count', posts.get_count_posts, methods=['GET'])

apiv1.add_url_rule('/posts', posts.insert_posts, methods=['POST'])
apiv1.add_url_rule('/data', posts.insert_posts, methods=['POST'])