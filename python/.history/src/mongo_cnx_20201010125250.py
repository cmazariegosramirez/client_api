import pymongo

mongocnx = pymongo.MongoClient("mongodb://localhost:27017/")


class CollPosts:
    def __init__(self, autor, nota) -> None:
        self.autor = autor
        self.nota = nota
    
    @classmethod
    def from_json(klass, js: dict):
        return klass(js.get('autor',''),js.get('nota',''))
        
    def to_json(self):
        return { "autor": self.autor, "nota": self.nota }


class MySchema:
    def __init__(self) -> None:
        global mongocnx
        self.cnx = mongocnx
        self.db = mongocnx['sopes1']
        self.coll_posts = self.db['posts']

my_schema = MySchema()