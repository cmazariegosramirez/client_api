from flask import Flask, request
from flask_cors import CORS
from src import apiv2


app = Flask(__name__)
CORS(app, max_age=1728000)

app.register_blueprint(apiv2, url_prefix='/v2.1')